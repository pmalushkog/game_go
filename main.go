package main

import (
	"fmt"
	"sort"
	"strings"
)

// 1-Коридор 2-Кухня 3-Комната 4-Улица

type Location struct {
	where        string              // что за локация
	ways         map[int]string      // куда можно пойти
	items        map[int]SpecialItem // вещи в комнате
	possibleDeed map[int]string      // какие действия возможны
	description  string
	duties       SpecialItem
	permission   bool
}

type SpecialItem struct {
	name     string
	contains map[int]string
}

type Character struct {
	Backpack []string // есть ли рюкзак и что в нем
	Place    Location
	PO       []string
}

// WallE Персонаж
var WallE Character

//  Локации
var Hall Location
var Room Location
var Kitchen Location
var Street Location

var world []Location // список локаций мира

// Предметы
var table SpecialItem
var tableRoom SpecialItem
var chair SpecialItem
var lookKitchen SpecialItem

// Глобальные переменные
var answDeed string
var answTemp string
var answTemp2 string

var mainAnswer string

const str = "улица"

func describing() string {
	var printerDescribing string

	printerDescribing += WallE.Place.description
	printerDescribing += " "

	return printerDescribing
}

func Usage(answU, answTemp2 string) string {
	var printerUsage string

	var existing int
	for val := range WallE.Backpack {
		if answU == WallE.Backpack[val] {
			existing = 1
			break
		}
	}
	if existing == 0 {
		printerUsage += "нет предмета в инвентаре - "
		printerUsage += answU
		return printerUsage

	}

	if answU == "ключи" && answTemp2 == "дверь" && existing != 0 {
		Street.permission = true
		printerUsage += "дверь открыта"
	} else if existing == 1 && answTemp2 != "дверь" {
		printerUsage += "не к чему применить"
	}
	return printerUsage
}

func Take(answT string) string {
	var printerTake string

	if WallE.Backpack != nil {

		var ui int
		for key := range WallE.Place.items {

			temp := WallE.Place.items[key]

			for key := range temp.contains {
				if answT == temp.contains[key] {

					ui += 1
					WallE.Backpack = append(WallE.Backpack, temp.contains[key])

					printerTake += "предмет добавлен в инвентарь: "
					printerTake += temp.contains[key]

					delete(temp.contains, key)
					WallE.Place.items[key] = temp
				}
			}
		}

		if ui == 0 {
			printerTake = "нет такого"
		}
	} else {
		printerTake = "некуда класть"
	}
	return printerTake
}

func PutOn(answPO string) string {
	var printerPutOn string
	fmt.Scan(&answPO)

	var markerPO int
	var markerC int

	for temp := range WallE.PO {
		if answPO == WallE.PO[temp] {
			markerPO = 1
			break
		}
	}

	for i := range WallE.Place.items {
		for i2 := range WallE.Place.items[i].contains {
			if answPO == WallE.Place.items[i].contains[i2] {
				markerC = 1

				if markerPO != 1 {
					delete(WallE.Place.items[i].contains, i2)
				}
				break
			}
		}
	}

	if markerPO != 1 && markerC == 1 {

		printerPutOn += "вы надели: "
		printerPutOn += answPO

		WallE.PO = append(WallE.PO, answPO)

		if answPO == "рюкзак" {

			WallE.Backpack = []string{}

			for key := range Kitchen.duties.contains {

				if Kitchen.duties.contains[key] == "собрать рюкзак и " {

					delete(Kitchen.duties.contains, key)

				}
			}

		}
	}
	return printerPutOn
}

func GoTo(answChoose string) string {
	var printerGo string
	if answChoose == WallE.Place.where {

		printerGo += "вы уже в "
		printerGo += answChoose

	} else {
		var ui int
		for key := range WallE.Place.ways {
			if answChoose == WallE.Place.ways[key] {
				ui += 1
				for temp := range world {
					if answChoose == world[temp].where {
						if answChoose == str && !Street.permission {
							return "дверь закрыта"

						} else {
							WallE.Place = world[temp]
							printerGo += describing()
						}
					}

				}
			}

		}
		if ui == 0 {
			printerGo += "нет пути в "
			printerGo += answChoose
			return printerGo
		}
	}
	printerGo += MozhnoProity()
	return printerGo
}

func LookAround() string {

	var printerLookAround string

	var i2 int
	var temp int
	var keyt int

	keyt += 1

	for key := range WallE.Place.items {
		if len(WallE.Place.items[key].contains) != 0 {
			temp += 1
		}
	}

	if temp == 0 {
		printerLookAround += "пустая комната. "
		printerLookAround += MozhnoProity()
		return printerLookAround
	}
	for keyt <= len(WallE.Place.items) {

		if WallE.Place.items[keyt].contains != nil && len(WallE.Place.items[keyt].contains) != 0 {

			printerLookAround += WallE.Place.items[keyt].name
			a := WallE.Place.items[keyt].contains
			var i int
			sl := []int{}

			for key := range a {
				sl = append(sl, key)
			}
			sort.Ints(sl)

			for key := range sl {
				printerLookAround += a[sl[key]]

				if i != len(a)-1 {
					printerLookAround += ", "
				}
				i++

			}
		}
		i2++
		if i2 != len(WallE.Place.items) && len(WallE.Place.items[keyt+1].contains) != 0 {
			printerLookAround += ", "

		}

		keyt++

	}
	if WallE.Place.duties.contains != nil {
		printerLookAround += ", "
		printerLookAround += WallE.Place.duties.name

		ssl := []int{}

		for key := range WallE.Place.duties.contains {
			ssl = append(ssl, key)
		}
		sort.Ints(ssl)

		for key := range ssl {
			printerLookAround += WallE.Place.duties.contains[ssl[key]]

		}
	}
	printerLookAround += ". "

	printerLookAround += MozhnoProity()

	return printerLookAround
}

func MozhnoProity() string {

	var printerMozhno string
	var i int
	var key = 1
	if WallE.Place.where == "улица" {
		return "можно пройти - домой"
	} else {
		printerMozhno += "можно пройти - "
		for key <= len(WallE.Place.ways) {

			i++
			printerMozhno += WallE.Place.ways[key]

			if i < len(WallE.Place.ways) {
				printerMozhno += ", "
			}
			key++

		}
	}
	return printerMozhno
}

func main() {
	initGame()
	for {
		fmt.Scanf("%s", &answDeed)
		handleCommand(answDeed)
	}
}

func initGame() {

	lookKitchen.name = ""
	lookKitchen.contains = map[int]string{
		1: "ты находишься на кухне",
	}

	table.name = "на столе: "
	table.contains = map[int]string{
		1: "чай",
	}

	chair.name = "на стуле: "
	chair.contains = map[int]string{
		1: "рюкзак",
	}

	tableRoom.name = "на столе: "
	tableRoom.contains = map[int]string{
		1: "ключи",
		2: "конспекты",
	}

	Hall.where = "коридор"
	Hall.ways = map[int]string{

		1: "кухня",
		2: "комната",
		3: "улица",
	}

	Hall.description = "ничего интересного."

	Hall.items = map[int]SpecialItem{}

	Hall.possibleDeed = map[int]string{
		1: "идти",
		2: "применить",
	}

	Room.where = "комната"
	Room.ways = map[int]string{

		1: "коридор",
	}

	Room.items = map[int]SpecialItem{
		1: tableRoom,
		2: chair,
	}

	Room.description = "ты в своей комнате."

	Room.possibleDeed = map[int]string{
		1: "осмотреться",
		2: "идти",
		3: "надеть",
		4: "взять",
		5: "применить",
	}
	Kitchen.where = "кухня"

	Kitchen.ways = map[int]string{

		1: "коридор",
	}

	Kitchen.items = map[int]SpecialItem{
		1: lookKitchen,
		2: table,
	}

	Kitchen.duties.name = "надо "
	Kitchen.duties.contains = map[int]string{
		1: "собрать рюкзак и ",
		2: "идти в универ",
	}

	Kitchen.description = "кухня, ничего интересного."

	Kitchen.possibleDeed = map[int]string{
		1: "осмотреться",
		2: "идти",
		3: "надеть",
		4: "взять",
		5: "применить",
	}

	Street.where = "улица"
	Street.ways = map[int]string{

		1: "коридор",
	}

	Street.description = "на улице весна."

	Street.items = map[int]SpecialItem{}

	Street.possibleDeed = map[int]string{
		1: "осмотреться",
		2: "идти",
		5: "применить",
	}
	Street.permission = false

	world = []Location{Hall, Street, Kitchen, Room}

	WallE.Place = Kitchen
	WallE.Backpack = nil
	WallE.PO = []string{}

}

func handleCommand(answDeed1 string) string {

	s := strings.Split(answDeed1, " ")
	answDeed12 := s[0]

	switch answDeed12 {

	case "идти":
		answTemp = s[1]
		mainAnswer = GoTo(answTemp)

		return mainAnswer

	case "осмотреться":
		mainAnswer = LookAround()

		return mainAnswer

	case "надеть":
		answTemp = s[1]
		mainAnswer = PutOn(answTemp)

		return mainAnswer

	case "применить":
		answTemp = s[1]
		answTempAdd := s[2]
		mainAnswer = Usage(answTemp, answTempAdd)

		return mainAnswer
	case "взять":
		answTemp = s[1]
		mainAnswer = Take(answTemp)

		return mainAnswer
	default:
		return "неизвестная команда"

	}

}